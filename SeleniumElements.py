from selenium import webdriver
import time

driver = webdriver.Chrome()
driver.get('https://www.selenium.dev/')

element = driver.find_element_by_xpath('/html/body/section[2]/div/div[1]/div[1]/img')

element.Click()
driver.back()

search_element = driver.find_element_by_id('q')

search_element.send_keys('webdriver')

go_button = driver.find_element_by_id('submit')

go_button.click()

time.sleep(1)

driver.switch_to.frame('master-1')
link_elements = driver.find_element_by_tag_name('a')

print(link_elements[0].get_attribute('href'))

